/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsiquet <lsiquet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/12 11:25:44 by lsiquet           #+#    #+#             */
/*   Updated: 2018/07/15 18:27:16 by lsiquet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "../../headers/ft.h"

int		ft_strcont(char *str, char c)
{
	while (*str)
	{
		if (c == *str)
			return (1);
		str++;
	}
	return (0);
}

int		ft_count_word_chars(char *str, char *charset)
{
	int c;

	c = 0;
	while (*str && ft_strcont(charset, *str))
	{
		c++;
		str++;
	}
	return (c);
}

int		ft_count_words(char *str, char *charset)
{
	int w_count;
	int is_word;

	w_count = 0;
	is_word = 0;
	while (*str)
	{
		if (ft_strcont(charset, *str))
		{
			if (is_word)
				w_count++;
			is_word = 0;
		}
		else
			is_word = 1;
		str++;
	}
	if (is_word)
		w_count++;
	return (w_count);
}

char	**ft_split(char *str, char *charset)
{
	char	**tab;
	int		w_count;
	int		i;
	char	*s;
	int		c;

	w_count = ft_count_words(str, charset);
	tab = malloc(sizeof(char *) * (w_count + 1));
	c = 0;
	while (c < w_count)
	{
		i = 0;
		s = malloc(sizeof(char) * (ft_count_word_chars(str, charset) + 1));
		while (*str && !ft_strcont(charset, *str))
		{
			s[i++] = *str;
			str++;
		}
		s[i] = '\0';
		str++;
		((i != 0) && (tab[c++] = s));
	}
	tab[c] = NULL;
	return (tab);
}
