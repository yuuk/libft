/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsiquet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/09 11:01:53 by lsiquet           #+#    #+#             */
/*   Updated: 2018/07/09 19:03:03 by lsiquet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strstr(char *str, char *to_find)
{
	int i;
	int ok;

	ok = 1;
	while (*str)
	{
		i = 0;
		while (to_find[i] != '\0')
		{
			if (str[i] == to_find[i])
				ok = 1;
			else
			{
				ok = 0;
				break ;
			}
			i++;
		}
		if (ok)
			return (str);
		str++;
	}
	return (0);
}
