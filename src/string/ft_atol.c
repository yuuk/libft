/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsiquet <lsiquet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/05 15:25:37 by lsiquet           #+#    #+#             */
/*   Updated: 2018/07/15 18:17:31 by lsiquet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_atoi(char *str)
{
	long response;
	int sign;

	sign = 1;
	response = 0;
	while (*str == '\t' || *str == '\v' || *str == '\n'||
		   *str == '\r' || *str == '\f' || *str == ' ')
	{
		str++;
	}
	if (str[0] == '+')
		str++;
	else if (str[0] == '-')
	{
		sign = -1;
		str++;
	}
	while (*str >= '0' && *str <= '9')
	{
		response = response * 10 + (*str - 48);
		str++;
	}
	return ((long)(response * sign));
}
