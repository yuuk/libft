/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsiquet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/09 15:37:41 by lsiquet           #+#    #+#             */
/*   Updated: 2018/07/09 19:39:05 by lsiquet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcapitalize(char *str)
{
	int		is_new_word;
	char	*str_ptr;

	str_ptr = str;
	is_new_word = 1;
	while (*str)
	{
		if ((*str >= '0' && *str <= '9') || (*str >= 'A' && *str <= 'Z'))
		{
			if (*str >= 'A' && *str <= 'Z' && !is_new_word)
				*str += 32;
			is_new_word = 0;
		}
		else if (*str >= 'a' && *str <= 'z')
		{
			if (is_new_word)
				*str -= 32;
			is_new_word = 0;
		}
		else
			is_new_word = 1;
		str++;
	}
	return (str_ptr);
}
