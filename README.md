 
                        /(((((((/    ,((((((((((((,                
                     .(&&&&&&&&&(    /&&&&&&&&&&&&&              
                    (&&&&&&&&&&&(    /&&&&&&&&&&&&&&&&*            
                 .(&&&&&&&&&&&&&(    *&&&&&&%#####&&&&&&*          
                /%%%%%%%&&&&&&&&(     .%&&&&&,    #&&&&          
                        #&&&&&&&(       .%&&&&&(  #&&&&          
                        #&&&&&&&(          #&&&&&(#&&&&          
                        #&&&&&&&(                 #&&&&          
                        #&&&&&&&(                 #&&&&          
                        #&&&&&&&(                 #&&&&          
                        #&&&&&&&(     %&&&&&&&&&&&&&&&&          
                        #&&&&&&&(      *&&&&&&&&&&&&&&&          
                        #&&&&&&&(        *&&&&&&&&&&&&&          
                        #&&&&&&&(          .////////////*          
                        #&&&&&&&(                                  
                        #&&&&&&&(                                  
                        #&&&&&&&(                                  

                      _____      _   __           ___  _    
                     |_   _|    (_) [  |        .' ..]/ |_  
                       | |      __   | |.--.   _| |_ `| |-' 
                       | |   _ [  |  | '/'`\ \'-| |-' | |   
                      _| |__/ | | |  |  \__/ |  | |   | |,  
                     |________|[___][__;.__.'  [___]  \__/  

        ==============================================================

    Here is a custom lib based on most of the functions developped during
     42 and 19 schools' "piscine" exams. It is supposed to help students
      to accelerate the tests and the developpement of their own codes.
      
      
                                HOW TO INSTALL
                                ==============
        Simply go to the lib's folder and execute 'sh libft_creator.sh' 
      in your terminal. You should then see appear 'libft.a' in the 'lib'
     directory. You also may use the .h  files in the 'headers' directory.
     
        Don't forget to place the .a file in the compilation directory and
                 to compile your code with the -L . -lft flag.
    