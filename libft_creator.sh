gcc -c src/*/ft_*.c
rm -rf lib
mkdir lib
ar rc lib/libft.a ft_*.o
ranlib lib/libft.a
rm *.o
